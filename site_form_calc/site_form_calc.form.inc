<?php

/**
 * Форма Калькулятор услуг
 */
function site_form_calc_form($form, &$form_state) {
  
  $form['#attached']['libraries_load'][] = array('angularjs');

  $form['#attributes'] = array(
	'name' => 'calc',
	'id' => 'pr-local-settings-f-calc-form' 
  );

  $sections_object = new prTaxonomy('service_section');
  $sections_tree = $sections_object->getTree();
  unset($sections_object);
  
  $form['#attributes']['class'][] = 'us-service-form';
  $form['#attributes']['class'][] = 'site_forms_api';
  
  // список разделов услуг как филдсеты
  foreach ($sections_tree as $section) {
    $nids = prTaxonomy::getNidsByTid($section->tid);
    // если в разделе есть услуги - выводим раздели и услуги
    if(!empty($nids)){
      
      $polosa = true;
      
      // раздел - филдсет
      $form['sections']['section_'.$section->tid] = array(
        '#type' => 'fieldset',
        '#description' => 
          '<div class="u-mt-30 u-mb-20">'
            . '<h2>'.$section->name.'</h2>'
            .prTaxonomy::getTermBodyByTid($section->tid)
          .'</div>',
      );

      if($section->tid == 11){

        // если это базовые сайты - выводим радиокнопки для каждой услуги
        
        // перечень услуг
        foreach ($nids as $nid) {
          $service = prServiceGetObject($nid);
          $options[$nid] = '';
        }
        // элемент формы с услугами - радиокнопками
        $form['sections']['section_'.$section->tid]['service_sites'] = array(
          '#type' => 'radios',
          '#options' => $options,
          '#default_value' => 86,
        );
        // добавляем префикс и суффикс
        foreach ($nids as $nid) {
          $service = prServiceGetObject($nid);
          $form['sections']['section_'.$section->tid]['service_sites'][$nid]['#prefix'] = _service_prefix($service, $polosa);
          $form['sections']['section_'.$section->tid]['service_sites'][$nid]['#suffix'] = _service_suffix($service);
          $polosa = !$polosa;
        }
        
      }else{
        // выводим чекбоксы для каждой услуги
        foreach ($nids as $nid) {
          $service = prServiceGetObject($nid);
          $form['sections']['section_'.$section->tid]['service_'.$nid] = array(
            '#type' => 'checkbox',
            '#prefix' => _service_prefix($service, $polosa),
            '#suffix' => _service_suffix($service),
          );
          $polosa = !$polosa;
        }
      }
      
    }
    
  }

  // раздел - филдсет
  $form['sections']['section_contacts'] = array(
    '#type' => 'fieldset',
    '#description' => 
      '<div class="u-mt-30 u-mb-20">'
        . '<h2>Отправить пожеления</h2>'
      .'</div>',
  );
	
	//описание для формы
	$form_descr = variable_get('it4r_service_admform_descr', '');
	if($form_descr != ''){
	  $form['sections']['section_contacts']['request_descr'] = array(
	  '#type'=> 'item',
	  '#markup'=> $form_descr,
	  );
	}
  
    //имя
    $form['sections']['section_contacts']['request_name'] = array(
	  '#type' => 'textfield',
	  '#title' => t('What is your name'),
	  '#required' => TRUE,
	  '#field_suffix' => '<span style="color:red" ng-show="submitted && calc.request_name.$error.required">This field is required</span>',
	  '#pre_fa_icon' => 'fa-user',
	  '#attributes' => array(
		'ng-model' => 'request_name'
	  ),
    );
    
    //почта
    $form['sections']['section_contacts']['request_email'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Your e-mail'),
	  '#required' => TRUE,
	  '#pre_fa_icon' => 'fa-envelope',
	  '#email' => TRUE,
	  '#field_suffix' => '<span style="color:red" ng-show="submitted && calc.request_email.$error.required">Email is required.</span>
						  <span style="color:red" ng-show="submitted && calc.request_email.$error.email">Invalid email address.</span>',
	  '#attributes' => array(
		'ng-model' => 'request_email'
	  ),
    );

    //телефон
    $form['sections']['section_contacts']['request_phone'] = array(
        '#type' => 'textfield',
        '#title' => t('Your phone'),
        '#required' => TRUE,
        '#pre_fa_icon' => 'fa-phone',
  );
    
    //вопрос
    $form['sections']['section_contacts']['request_request'] = array(
        '#type' => 'textarea',
        '#title' => t('Question').', '.t('Message').'...',
    );
    
    //recapcha
    $form['sections']['section_contacts']['captcha'] = array(
      '#type'=> 'item',
      '#markup'=>''
    );
  
  
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
	'#attributes' => array(
	  'ng-click' => 'submitted=true',
	),
  );
  
  return $form;
  
}

/**
 * Функция submit для формы "Заявка на услугу"
 */
function site_form_calc_form_submit($form, &$form_state) {
  
  $values = $form_state['values'];
  
  $forms_api = new siteFormsApiBase();
  
  //сформируем html отображение данных формы
  $human_view = _human_view($values);
  
  $recipient_uid = variable_get('it4r_service_admform_recipient', '');
  // добавление в заполненные формы
  $data = array(
	'form_type' => 'calculator',
	'form_recipient' => $recipient_uid,
	'sender_email' => $values['request_email'],
	'sender_name' => $values['request_name'],
	'sender_phone' => $values['request_phone'],
	'sender_file' => '',
	'human_view' => $human_view, 
	'form_serialize' => $values
  );
  $forms_api->addForm($data);
  
  $form_info = '';
  
  if(!empty($values['request_name'])){
	$form_info .= '<p>';
	  $form_info .= '<label>'.t('it4r_all_admform_name').': '.'</label>';
	  $form_info .= '<span>'.$values['request_name'].'</span>';
	$form_info .= '</p>';
  }
  
  if(!empty($values['request_email'])){
	$form_info .= '<p>';
	  $form_info .= '<label>'.t('it4r_all_admform_email').': '.'</label>';
	  $form_info .= '<span>'.$values['request_email'].'</span>';
	$form_info .= '</p>';
  }
  
  if(!empty($values['request_phone'])){
	$form_info .= '<p>';
	  $form_info .= '<label>'.t('it4r_all_admform_phone').': '.'</label>';
	  $form_info .= '<span>'.$values['request_phone'].'</span>';
	$form_info .= '</p>';
  }
  
  // отправка посетителю сайта
  $visitor_body = variable_get('it4r_service_admform_visitorbody', '');
  
  $visitor_body_letter = '';
  if(isset($visitor_body['value'])){
	$visitor_body_letter .= $visitor_body['value'];
  }
  $visitor_body_letter .= $form_info;
  $visitor_body_letter .= $human_view;
	
  $forms_api->sendMail(
	'calculator', 
	$values['request_name'].' '.'<'.$values['request_email'].'>', 
	variable_get('site_name').' '.'<'.variable_get('site_mail').'>', 
	array(
	  'subject' => variable_get('it4r_service_admform_visitorsubject', ''),
	  'body' => $visitor_body_letter
	)
  );
  
  //отправка получателю сайта
  //если заполненный получатель в настройках
  if(is_numeric($recipient_uid)){
	$recipient_body = variable_get('it4r_service_admform_recipientbody', '');
	$recipient_user = user_load($recipient_uid);
	
	$recipient_body_letter = '';
	if(isset($recipient_body['value'])){
	  $recipient_body_letter .= $recipient_body['value'];
	}
	$recipient_body_letter .= $form_info;
	$recipient_body_letter .= $human_view;
	// отправка посетителю сайта
	$forms_api->sendMail(
	  'calculator', 
	  $recipient_user->name.' '.'<'.$recipient_user->mail.'>', 
	  variable_get('site_name').' '.'<'.variable_get('site_mail').'>', 
	  array(
		'subject' => variable_get('it4r_service_admform_recipientsubject', ''),
		'body' => $recipient_body_letter
	  )
	);
  }
  
  //если есть данные о сообщении на сайт 
  $message_site = variable_get('it4r_service_admform_message', '');
  if($message_site != ''){
	drupal_set_message($message_site);
  }
  
}

/**
 * Функция валидации формы "Заявка на услугу"
 */
function site_form_calc_form_validate($form, &$form_state) {
  
}

function _human_view($form_param){
  
  //получим данные о заказе
  $order = ''; $currency_label = '$';
  foreach ($form_param as $key => $select) {
	if((strpos($key,'service_') !== false) && ($select == 1)){
	  $nid_service = str_replace('service_', '', $key);
	  $service = prServiceGetObject($nid_service);
	  $order .= '<p>';
		$order .= '<label>'.$service->getTitle()->val[0]['value'].': '.'</label>';
		$order .= '<span>'.$currency_label.$service->getPrice()->val[0]['value'].'</span>';
	  $order .= '</p>';
	}
	if($key == 'service_sites'){
	  $service = prServiceGetObject($select);
	  $order .= '<p>';
		$order .= '<label>'.$service->getTitle()->val[0]['value'].': '.'</label>';
		$order .= '<span>'.$currency_label.$service->getPrice()->val[0]['value'].'</span>';
	  $order .= '</p>';
	}
	
  }
  
  $output = '';
  
  //заказ
  if(!empty($order)){
	$output .= '<p>';
	  $output .= '<label>Продукты и услуги: </label>';
	$output .= '</p>';
	$output .= $order;
  }
  
  //вопрос, сообщение
  if(!empty($form_param['request_request'])){
	$output .= '<p>';
	  $output .= '<label>'.t('it4r_all_admform_questionmessage').': '.'</label>';
	$output .= '</p>';
	$output .= '<p>';
	  $output .= '<span>'.$form_param['request_request'].'</span>';
	$output .= '</p>';
  }
  
  
  return $output;
  
}


/**
 * Функция темизации для формы "Заявка на услугу"
 */
function _service_prefix($service, $polosa){
  $output = '<div class="row'.($polosa ? ' row-odd' : '').'">';
  
  $output .= 
      '<div class="col-md-8">
        <h3 class="u-service-title">
          <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg-'.$service->getNid()->val[0]['value'].'">
            '.$service->getTitle()->val[0]['value'].'    
          </a>
        </h3>
        <div class="u-service-sh-descr">
          '.  truncate_utf8($service->getShortDescr()->val[0]['value'], 130).'
        </div>
      </div>';
  
  $output .= 
      '<div class="col-md-1">
        <div class="u-service-info">
          <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg-'.$service->getNid()->val[0]['value'].'">
            <i class="fa fa-file-text-o"></i>          
          </a>
        </div>
      </div>';
  
  $price_old = '';
  if(isset($service->getPriceOld()->val[0]['value'])){
    $price_old =        
      '<div class="u-service-price-old">
        $'.$service->getPriceOld()->val[0]['value'].'
      </div>';
  }
  $output .= 
      '<div class="col-md-2">
        <div class="u-service-price">
          $'.$service->getPrice()->val[0]['value'].'    
        </div>
        '.$price_old.'
      </div>';

  $output .= 
      '<div class="col-md-1">
        <div class="u-service-select">';
  
  return $output;
}

/**
 * Функция темизации для формы "Заявка на услугу"
 */
function _service_suffix($service){
  $output = '</div></div></div>';
  
  $output .= 
      '<div class="modal fade bs-example-modal-lg-'.$service->getNid()->val[0]['value'].'" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                      <h4 id="myLargeModalLabel2" class="modal-title">'.$service->getTitle()->val[0]['value'].'</h4>
                  </div>
                  <div class="modal-body">
                      '.$service->getBody()->val[0]['value'].'
                  </div>
              </div>
          </div>
      </div>';

  return $output;
}