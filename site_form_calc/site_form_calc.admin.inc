<?php
/**
коммит в девелоп 1
 */

// хотфикс 6 для мастер

/**
 * коммит в девелоп 2
 */

// хостфикс 4 из мастера

/**
 * Настроечная форма отправки писем для заявок Услуг
 */
function site_form_calc_form_admin_form($form, &$form_state){
  
  /**
   * хотфикс 1
   */
  
  //справочный текст
  $form['it4r_service_admform_descr'] = array(
	'#type' => 'textfield',
	'#title' => t('it4r_service_admform_descr'),
	'#default_value' => variable_get('it4r_service_admform_descr', '')
  );
  
  /**
   * хотфикс 2
   */
      
  //сообщение после отправки письма
  $form['it4r_service_admform_message'] = array(
	'#type' => 'textfield',
	'#title' => t('it4r_service_admform_message'),
	'#default_value' => variable_get('it4r_service_admform_message', '')
  );
  
  //филдсет настройки сообщение для поситителя сайта
  $form['form_visitor'] = array(
	'#type' => 'fieldset',
	'#title' => t('it4r_all_admform_visitor'), 
	'#collapsible' => TRUE,
	'#collapsed' => TRUE,
  ); 
  
  /**
   * хотфикс 3 из мастер
   */
  
  //тема письма для получателя
  $form['form_visitor']['it4r_service_admform_visitorsubject'] = array(
	'#type' => 'textfield',
	'#title' => t('it4r_all_admform_subject'),
	'#default_value' => variable_get('it4r_service_admform_visitorsubject', ''), 
  );

  //тело письма для получателя
  $visitor_body = variable_get('it4r_service_admform_visitorbody', '');
  $form['form_visitor']['it4r_service_admform_visitorbody'] = array(
	'#type' => 'text_format',
	'#title' => t('it4r_all_admform_body'),
	'#base_type' => 'textarea',
	'#rows' => 10,
	'#default_value' => (isset($visitor_body['value']) ? $visitor_body['value'] : ''), 
  );
  
  //филдсет настройки сообщение для получателя
  $form['form_recipient'] = array(
	'#type' => 'fieldset',
	'#title' => t('it4r_all_admform_recipient'), 
	'#collapsible' => TRUE,
	'#collapsed' => TRUE,
  );
  
  //получатель
  $option_recipient = siteFormsApiBase::getFormRecipient();
  $form['form_recipient']['it4r_service_admform_recipient'] = array(
	'#type' => 'select',
	'#title' => t('it4r_all_admform_recipient'),
	'#options'=> $option_recipient,
	'#default_value' => variable_get('it4r_property_admform_recipientuid', ''),
  );
  
  //тема письма для администратору
  $form['form_recipient']['it4r_service_admform_recipientsubject'] = array(
	'#type' => 'textfield',
	'#title' => t('it4r_all_admform_subject'),
	'#default_value' => variable_get('it4r_service_admform_recipientsubject', ''), 
  );
        
  //тело письма для администратору
  $recipient_body = variable_get('it4r_service_admform_recipientbody', '');
  $form['form_recipient']['it4r_service_admform_recipientbody'] = array(
	'#type' => 'text_format',
	'#title' => t('it4r_all_admform_body'),
	'#base_type' => 'textarea',
	'#rows' => 10,
	'#default_value' => (isset($recipient_body['value']) ? $recipient_body['value'] : ''), 
  );
  
  return system_settings_form($form);
  
}
